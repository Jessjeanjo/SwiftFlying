Created by Jessica Joseph | 2015 05 May

A program for the FizzBuzz number game.
If the number is a multiple of 3, "Fizz" is printed
If the number is a multiple of 5, "Buzz" is printed
If the number is a multiple of 3 and 5, "FizzBuzz" is printed
Otherwise, the number is printed
 
 for numb in 1...100 {
    if numb % 3 == 0{
        if numb % 5 == 0 {
            println("FizzBuzz")
        }
    println("Fizz")
    }
    else if numb % 5 == 0{
        println("Buzz")
    }
    else{
        println(numb)
    }
}
